import { useEffect, useRef, useState } from "react";
import "./App.css";

function App() {
  const [time, settime] = useState(0);

  useEffect(() => {
    return () => clearInterval(id.current);
  }, []);

  let id = useRef();

  function handelTime() {
    id.current = setInterval(() => {
      settime((prev) => prev + 1);
      console.log(time);
    }, 1000);
  }

  return (
    <div className="App">
      <h1>counter</h1>
      <h1>{time}</h1>
      <button onClick={() => handelTime()}>Start</button>
      <button onClick={() => clearInterval(id.current)}>stop</button>
      <button onClick={() =>{ clearInterval(id.current);
          settime(0);
        }}
      >
        Reset
      </button>
    </div>
  );
}

export default App;